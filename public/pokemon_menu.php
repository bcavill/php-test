<?php

    $offset = '';
    if (isset($_REQUEST['offset']))
    {
        $offset = $_REQUEST['offset'];
    }

    $pokemon = json_decode($api->pokemon("?offset=$offset"), 'true');

    foreach($pokemon['results'] as $pocket_monster)
    {
        echo '<ul id="pokemon"><li><a href="?pokemon=', $pocket_monster['name'], '">', htmlentities($pocket_monster['name']),  '</a></li></ul>';
    }
    unset($pocket_monster);

    if (@$pokemon['previous'] != null)
    {
        echo '<a href="?', substr($pokemon['previous'], strpos($pokemon['previous'], '&') + 1), '">Previous Page</a>';
    }

    if (@$pokemon['next'] != null)
    {
        echo '<a id="next" href="?', substr($pokemon['next'], strpos($pokemon['next'], '&') + 1), '">Next Page</a>';
    }
?>
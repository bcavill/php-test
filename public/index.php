<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Pokédex</title>
        <link href='css/style.css' rel='stylesheet'>
    </head>
    <body>
        <div id='pokedex'>
            <div id='speech_light_outer'>
                <div id='speech_light_inner'>
                </div>
            </div>
            <div id='screen'>
                <form>
                    Search:&nbsp;
                    <input name="pokemon" placeholder="Pikachu">
                    <input type="submit" value="Search">
                </form>
                <br>
                <?php
                    include '../vendor/autoload.php';
                    use PokePHP\PokeApi;
                    $api = new PokeApi;

                    if (isset($_REQUEST['pokemon']))
                    {
                        include 'pokemon.php';
                    }
                    else
                    {
                        include 'pokemon_menu.php';
                    }
                ?>
            </div>
        </div>
    </body>
</html>
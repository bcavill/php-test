<?php
    $pokemon = strtolower($_REQUEST['pokemon']);

    $data = json_decode($api->pokemon($pokemon));

    echo '<a href="index.php">Back to menu</a><br>';

    if ($data == 'An error has occured.')
    {
        echo 'Error! Pokemon not found.';
        exit();
    }

    echo '<h1>', ucfirst(htmlentities($data->name)), '</h1>';
    echo '<img src="', $data->sprites->front_default, '" alt="pokemon image">';
    echo '<p>Species: ', htmlentities($data->species->name), '</p>';
    echo '<p>Height: ', htmlentities($data->height / 10), 'M</p>';
    echo '<p>Weight: ', htmlentities($data->weight / 10), 'KG</p>';

    echo '<h2>Abilities</h2></ul>';
    foreach ($data->abilities as $ability)
    {
        echo '<li>', htmlentities($ability->ability->name), '</li>';
    }
    echo '</ul';
?>